# novamud-deployment

Instructions for how to set up your own deployment on Heroku

## Create a new repo

1. Copy the contents of `.gitlab-ci.yml` into a file of the same name in your new repo.
2. Copy the contents of the `Procfile` into a file of the same name in your new repo.

## Sign up and set up with heroku

It's cool and all that we can run the servers on our own machines. However, it doesn't
do much good in terms of making the model available to the rest of the world. All this
`localhost` stuff doesn't help anybody that's not typing on your local machine.

So let's take all of the work we've done getting this running and put it on heroku
where it can generate real business value. For this part, you can use any server
that has a static IP address though since we want to avoid the overhead of administering
our own server, we will use a servive to do this for us called [heroku](https://www.heroku.com/)
This is one of the oldest managed platforms out there and is quite robust, well-known, and
documented. However, be careful before you move forward with a big project o Heroku -
it can get CRAZY expensive REALLY fast.

However, for our purposes, they offer a free tier webserver and database that is enough to suit our
needs and we can do deployments in a few commands super easily. This may be a bit tough
for some of you but trust me: the alternative of admining your own server is MUCH more difficult.

### Sign up and set up at heroku

Go to the [signup page](https://signup.heroku.com/) and register for the free tier.

Once this is all done, go to the [dashboard](https://dashboard.heroku.com/apps) and create a new
app:

![create new app](https://i.imgur.com/WKTLhyC.png)

Then on the next screen, give it a name and make sure that it's in the Europe zone. It won't
kill anobody to have it in the land of the free but it's kinda far...

![select name and region](https://i.imgur.com/oUPNzOk.png)

Once this is done, select "create app" and you'll be sent to a page that's a bit intimidating
beacuse it just has a lot of stuff. Don't worry though, it's pretty simple what we need
to do next.

First up, make sure that you select the Heroku Git deployment method. It should already be selected
so I don't think you'll need to do anything.

![heroku git](https://i.imgur.com/xt0dAhq.png)

### Generate a heroku API key

1. Click on the top right menu of your heroku account and select "Settings"
2. Then click "generate api key"
3. Copy this api key somewhere secure that you will then delete from later on

## Setting the API key in your gitlab account

Do all of the following in your gitlab project

1. Select "Settings" -> "CI / CD"
2. Click "Expand"  under the "Variables" section
3. Enter in your `HEROKU_PRODUCTION_API_KEY` as the "input variable key" and
   the api key that you copied from the previous step as the value.
4. Then hit "save variables"
5. Open up your `.gitlab-ci.yml` again and replace where it says <app-name>
   with your actual <app-name>

## Deploy your game

Now if you have a dungeon implemented in a file called `my_dungeon.py` when
you push to master, it will trigger a build and deploy to heroku which will
make your game available to the entire internet!


